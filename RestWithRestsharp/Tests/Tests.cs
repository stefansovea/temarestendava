﻿using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using RestWithRestsharp.Models;
using System;
using System.Collections.Generic;

namespace RestWithRestsharp.Tests
{
    class Tests
    {
        private const string Host = "https://jsonplaceholder.typicode.com";
        private RestClient _restClient;

        [SetUp]
        public void Setup()
        {
            _restClient = new RestClient(Host);
        }

        [Test]
        public void VerifyThatTheNumberOfTodosForUserIdOneIsEven()
        {
            // Arrange
            var request = new RestRequest("/todos", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);
            int paritate = response.Count % 2;
            // Assert
            Assert.That(response, Has.Count.GreaterThan(0));
            Assert.That(paritate, Is.EqualTo(0));
        }

        [Test]
        public void VerifyThatTheNumberOfIdIsLowerThan9()
        {
            // Arrange
            var request = new RestRequest("/todos", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);

            int max = 0;
            foreach (var x in response)
            {
                if (max < x.UserId)
                    max = x.UserId;
            }

            // Assert
            Assert.That(max, Is.LessThan(10));
        }

        [Test]
        public void VerifyThatTheNumberOfIdIsLowerThan80()
        {
            // Arrange
            var request = new RestRequest("/todos", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);

            int max = 0;
            foreach (var x in response)
            {
                if (max < x.UserId)
                    max = x.UserId;
            }

            // Assert
            Assert.That(max, Is.LessThan(81));
        }

        [Test]
        public void GetMaximumIdComments()
        {
            // Arrange
            var request = new RestRequest("/comments", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);

            int max = 0;
            foreach (var x in response)
            {
                if (max < x.Id)
                    max = x.Id;
            }

            // Assert
           Assert.Pass("Succes {0}", max);
        }

        [Test]
        public void GetPhotosId1()
        {
            // Arrange
            var request = new RestRequest("/photos", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);

            int nr = 0;
            foreach (var x in response)
            {
                if (x.Id == 1)
                    nr++;
            }

            // Assert
            Assert.Pass("Succes {0}", nr);
        }

        [Test]
        public void GetAlbumUser4()
        {
            // Arrange
            var request = new RestRequest("/albums", Method.GET);


            // Act
            var restSharpResponse = _restClient.Execute(request);
            var response = JsonConvert.DeserializeObject<List<Todo>>(restSharpResponse.Content);

            int nr = 0;
            foreach (var x in response)
            {
                if (x.UserId == 4)
                    nr++;
            }

            // Assert
            Assert.Pass("Succes {0}", nr);
        }

       
    }
}